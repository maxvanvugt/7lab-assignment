import Profile from '../pages/profile'
import Products from '../pages/products'
import Login from '../pages/login';

const routes = [
  {
    path: '/',
    component: () => import('../layouts/auth.vue'),
    children: [
      { path: '/', component: Login, name: 'login' },
    ]
  },
  {
    path: '/main',
    component: () => import('../layouts/application.vue'),
    children: [
      { path: '/profile', component: Profile, name: 'profile' },
      { path: '/products', component: Products, name: 'products'},
    ]
  },
];

export default routes;