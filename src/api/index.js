let user = JSON.parse(localStorage.getItem('user'));
let url = 'https://assessment.7apps.nl/api'

export async function signIn (username, password) {
  const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json', "Accept": "application/json" },
      body: JSON.stringify({ email: username, password: password })
  };

  return fetch(`${url}/login`, requestOptions)
  .then(response => response.json())
  .then(json => {
      localStorage.setItem('user', JSON.stringify(json));
  });
}

export async function getProducts () {
  const requestOptions = {
      method: 'GET',
      headers: {'Authorization': 'Bearer ' + user.accessToken	, 'Content-Type': 'application/json' },
  }
  return fetch(`${url}/products`, requestOptions)
  .then(response => response.json())
  .then(json => {
    return json
  });
}

export async function createProduct (product) {
    const requestOptions = {
      method: 'POST',
      headers: {'Authorization': 'Bearer ' + user.accessToken	, 'Content-Type': 'application/json' },
      body: JSON.stringify({
        name: product.name,
        description: product.description,
        price: product.price,
        stock_id: product.stock.id
      })
    }
    return fetch(`${url}/products`, requestOptions)
    .then(response => response)
}