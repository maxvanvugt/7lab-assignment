import Vuex from 'vuex'
import Vue from 'vue'

import shopping from './shopping'

Vue.use(Vuex)

export default function () {
  const Store = new Vuex.Store({
    modules: {
      shopping
    },
  })

  return Store
}
